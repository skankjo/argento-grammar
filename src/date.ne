@builtin "whitespace.ne"

@{% var moment = require('moment'); %}
@{% const _ = require('ramda'); %}
@{% var join = _.pipe(_.flatten, _.join('')); %}
@{% const DATE_FORMAT = 'YYYY-MM-DD'; %}

date -> (date_of_month | iso_date | nl_date)

iso_date -> year "-" month "-" day {%  (d) => moment(d.join('')).format(DATE_FORMAT) %}

nl_date -> today | yesterday | days_ago | weeks_ago | months_ago | years_ago | for_time

year -> date_int date_int date_int date_int {% join %}

month -> date_int date_int {% join %}

day -> date_int date_int {% join %}

date_int -> [0-9] {% (d) => parseInt(d[0]) %}

date_of_month -> date_int:+  {% (d) => {
  const now = moment();
  const currentDateOfMonth = now.date();
  const givenDateOfMonth = parseInt(d[0].join(''));
  const date = givenDateOfMonth > currentDateOfMonth ? now.subtract(1, 'months').date(givenDateOfMonth) : now.date(givenDateOfMonth);
  return date.format(DATE_FORMAT);
} %}

today -> "today" {% (d) => moment().format(DATE_FORMAT) %}

yesterday -> "yesterday" {% (d) => moment().subtract(1, 'days').format(DATE_FORMAT) %}

a -> "a":? {% d => 1 %}

qty -> (date_int:+ | a) {% d => parseInt(join(d)) %}

days_ago -> qty _ days _ "ago" {% (d) => moment().subtract(parseInt(d[0]), d[2]).format(DATE_FORMAT) %}

weeks_ago -> qty _ weeks _ "ago" {% (d) => moment().subtract(parseInt(d[0]), d[2]).format(DATE_FORMAT) %}

months_ago -> qty _ months _ "ago" {% (d) => moment().subtract(parseInt(d[0]), d[2]).format(DATE_FORMAT) %}

years_ago -> qty _ years _ "ago" {% (d) => moment().subtract(parseInt(d[0]), d[2]).format(DATE_FORMAT) %}

for -> ("before" | "for") _ date_int:* _ ("last"|"past"):? {% (d) => d[2].length == 0 ? 1 : parseInt(d[2].join('')) %}

days -> "day" "s":? {% (d) => "days" %}

weeks -> "week" "s":? {% (d) => "weeks" %}

months -> "month" "s":? {% (d) => "months" %}

years -> "year" "s":? {% (d) => "years" %}

for_time -> for _ (days | weeks | months | years) {% (d) => (moment().subtract(d[0], d[2]).format(DATE_FORMAT)) %}
