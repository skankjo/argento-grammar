const test = require('ava');
const chrono = require('chronokinesis');
const { parseDate } = require('../dist');

const today = '2018-04-14';

test.before('Fix today date', t => chrono.freeze(today));

test.after('Reset the date', t => chrono.reset());

test('Should return today date', t => {
  const date = parseDate('today');
  t.is(date, today);
});

test('Should return yesterday', t => {
  const date = parseDate('yesterday');
  t.is(date, '2018-04-13');
});

test('Should return 1 day ago', t => {
  const date = parseDate('1 day ago');
  t.is(date, '2018-04-13');
});

test('Should give a day ago', t => {
  const date = parseDate('a day ago');
  t.is(date, '2018-04-13');
});

test('Should give "day ago"', t => {
  const date = parseDate('day ago');
  t.is(date, '2018-04-13');
});

test('Should give 2 days ago', t => {
  const date = parseDate('2 days ago');
  t.is(date, '2018-04-12');
});

test('Should give 15 days ago', t => {
  const date = parseDate('15 days ago');
  t.is(date, '2018-03-30');
});

test('Should give 1 month ago', t => {
  const date = parseDate('1 month ago');
  t.is(date, '2018-03-14');
});

test('Should give 1 year ago', t => {
  const date = parseDate('1 year ago');
  t.is(date, '2017-04-14');
});

test('Should give 100 years ago', t => {
  const date = parseDate('100 years ago');
  t.is(date, '1918-04-14');
});

test('Should give 1 week ago', t => {
  const date = parseDate('1 week ago');
  t.is(date, '2018-04-07');
});

test('Should give 2 weeks ago', t => {
  const date = parseDate('2 weeks ago');
  t.is(date, '2018-03-31');
});
