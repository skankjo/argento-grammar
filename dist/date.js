// Generated automatically by nearley, version 2.13.0
// http://github.com/Hardmath123/nearley
(function () {
function id(x) { return x[0]; }
 var moment = require('moment'); 
 const _ = require('ramda'); 
 var join = _.pipe(_.flatten, _.join('')); 
 const DATE_FORMAT = 'YYYY-MM-DD'; var grammar = {
    Lexer: undefined,
    ParserRules: [
    {"name": "_$ebnf$1", "symbols": []},
    {"name": "_$ebnf$1", "symbols": ["_$ebnf$1", "wschar"], "postprocess": function arrpush(d) {return d[0].concat([d[1]]);}},
    {"name": "_", "symbols": ["_$ebnf$1"], "postprocess": function(d) {return null;}},
    {"name": "__$ebnf$1", "symbols": ["wschar"]},
    {"name": "__$ebnf$1", "symbols": ["__$ebnf$1", "wschar"], "postprocess": function arrpush(d) {return d[0].concat([d[1]]);}},
    {"name": "__", "symbols": ["__$ebnf$1"], "postprocess": function(d) {return null;}},
    {"name": "wschar", "symbols": [/[ \t\n\v\f]/], "postprocess": id},
    {"name": "date$subexpression$1", "symbols": ["date_of_month"]},
    {"name": "date$subexpression$1", "symbols": ["iso_date"]},
    {"name": "date$subexpression$1", "symbols": ["nl_date"]},
    {"name": "date", "symbols": ["date$subexpression$1"]},
    {"name": "iso_date", "symbols": ["year", {"literal":"-"}, "month", {"literal":"-"}, "day"], "postprocess": (d) => moment(d.join('')).format(DATE_FORMAT)},
    {"name": "nl_date", "symbols": ["today"]},
    {"name": "nl_date", "symbols": ["yesterday"]},
    {"name": "nl_date", "symbols": ["days_ago"]},
    {"name": "nl_date", "symbols": ["weeks_ago"]},
    {"name": "nl_date", "symbols": ["months_ago"]},
    {"name": "nl_date", "symbols": ["years_ago"]},
    {"name": "nl_date", "symbols": ["for_time"]},
    {"name": "year", "symbols": ["date_int", "date_int", "date_int", "date_int"], "postprocess": join},
    {"name": "month", "symbols": ["date_int", "date_int"], "postprocess": join},
    {"name": "day", "symbols": ["date_int", "date_int"], "postprocess": join},
    {"name": "date_int", "symbols": [/[0-9]/], "postprocess": (d) => parseInt(d[0])},
    {"name": "date_of_month$ebnf$1", "symbols": ["date_int"]},
    {"name": "date_of_month$ebnf$1", "symbols": ["date_of_month$ebnf$1", "date_int"], "postprocess": function arrpush(d) {return d[0].concat([d[1]]);}},
    {"name": "date_of_month", "symbols": ["date_of_month$ebnf$1"], "postprocess":  (d) => {
          const now = moment();
          const currentDateOfMonth = now.date();
          const givenDateOfMonth = parseInt(d[0].join(''));
          const date = givenDateOfMonth > currentDateOfMonth ? now.subtract(1, 'months').date(givenDateOfMonth) : now.date(givenDateOfMonth);
          return date.format(DATE_FORMAT);
        } },
    {"name": "today$string$1", "symbols": [{"literal":"t"}, {"literal":"o"}, {"literal":"d"}, {"literal":"a"}, {"literal":"y"}], "postprocess": function joiner(d) {return d.join('');}},
    {"name": "today", "symbols": ["today$string$1"], "postprocess": (d) => moment().format(DATE_FORMAT)},
    {"name": "yesterday$string$1", "symbols": [{"literal":"y"}, {"literal":"e"}, {"literal":"s"}, {"literal":"t"}, {"literal":"e"}, {"literal":"r"}, {"literal":"d"}, {"literal":"a"}, {"literal":"y"}], "postprocess": function joiner(d) {return d.join('');}},
    {"name": "yesterday", "symbols": ["yesterday$string$1"], "postprocess": (d) => moment().subtract(1, 'days').format(DATE_FORMAT)},
    {"name": "a$ebnf$1", "symbols": [{"literal":"a"}], "postprocess": id},
    {"name": "a$ebnf$1", "symbols": [], "postprocess": function(d) {return null;}},
    {"name": "a", "symbols": ["a$ebnf$1"], "postprocess": d => 1},
    {"name": "qty$subexpression$1$ebnf$1", "symbols": ["date_int"]},
    {"name": "qty$subexpression$1$ebnf$1", "symbols": ["qty$subexpression$1$ebnf$1", "date_int"], "postprocess": function arrpush(d) {return d[0].concat([d[1]]);}},
    {"name": "qty$subexpression$1", "symbols": ["qty$subexpression$1$ebnf$1"]},
    {"name": "qty$subexpression$1", "symbols": ["a"]},
    {"name": "qty", "symbols": ["qty$subexpression$1"], "postprocess": d => parseInt(join(d))},
    {"name": "days_ago$string$1", "symbols": [{"literal":"a"}, {"literal":"g"}, {"literal":"o"}], "postprocess": function joiner(d) {return d.join('');}},
    {"name": "days_ago", "symbols": ["qty", "_", "days", "_", "days_ago$string$1"], "postprocess": (d) => moment().subtract(parseInt(d[0]), d[2]).format(DATE_FORMAT)},
    {"name": "weeks_ago$string$1", "symbols": [{"literal":"a"}, {"literal":"g"}, {"literal":"o"}], "postprocess": function joiner(d) {return d.join('');}},
    {"name": "weeks_ago", "symbols": ["qty", "_", "weeks", "_", "weeks_ago$string$1"], "postprocess": (d) => moment().subtract(parseInt(d[0]), d[2]).format(DATE_FORMAT)},
    {"name": "months_ago$string$1", "symbols": [{"literal":"a"}, {"literal":"g"}, {"literal":"o"}], "postprocess": function joiner(d) {return d.join('');}},
    {"name": "months_ago", "symbols": ["qty", "_", "months", "_", "months_ago$string$1"], "postprocess": (d) => moment().subtract(parseInt(d[0]), d[2]).format(DATE_FORMAT)},
    {"name": "years_ago$string$1", "symbols": [{"literal":"a"}, {"literal":"g"}, {"literal":"o"}], "postprocess": function joiner(d) {return d.join('');}},
    {"name": "years_ago", "symbols": ["qty", "_", "years", "_", "years_ago$string$1"], "postprocess": (d) => moment().subtract(parseInt(d[0]), d[2]).format(DATE_FORMAT)},
    {"name": "for$subexpression$1$string$1", "symbols": [{"literal":"b"}, {"literal":"e"}, {"literal":"f"}, {"literal":"o"}, {"literal":"r"}, {"literal":"e"}], "postprocess": function joiner(d) {return d.join('');}},
    {"name": "for$subexpression$1", "symbols": ["for$subexpression$1$string$1"]},
    {"name": "for$subexpression$1$string$2", "symbols": [{"literal":"f"}, {"literal":"o"}, {"literal":"r"}], "postprocess": function joiner(d) {return d.join('');}},
    {"name": "for$subexpression$1", "symbols": ["for$subexpression$1$string$2"]},
    {"name": "for$ebnf$1", "symbols": []},
    {"name": "for$ebnf$1", "symbols": ["for$ebnf$1", "date_int"], "postprocess": function arrpush(d) {return d[0].concat([d[1]]);}},
    {"name": "for$ebnf$2$subexpression$1$string$1", "symbols": [{"literal":"l"}, {"literal":"a"}, {"literal":"s"}, {"literal":"t"}], "postprocess": function joiner(d) {return d.join('');}},
    {"name": "for$ebnf$2$subexpression$1", "symbols": ["for$ebnf$2$subexpression$1$string$1"]},
    {"name": "for$ebnf$2$subexpression$1$string$2", "symbols": [{"literal":"p"}, {"literal":"a"}, {"literal":"s"}, {"literal":"t"}], "postprocess": function joiner(d) {return d.join('');}},
    {"name": "for$ebnf$2$subexpression$1", "symbols": ["for$ebnf$2$subexpression$1$string$2"]},
    {"name": "for$ebnf$2", "symbols": ["for$ebnf$2$subexpression$1"], "postprocess": id},
    {"name": "for$ebnf$2", "symbols": [], "postprocess": function(d) {return null;}},
    {"name": "for", "symbols": ["for$subexpression$1", "_", "for$ebnf$1", "_", "for$ebnf$2"], "postprocess": (d) => d[2].length == 0 ? 1 : parseInt(d[2].join(''))},
    {"name": "days$string$1", "symbols": [{"literal":"d"}, {"literal":"a"}, {"literal":"y"}], "postprocess": function joiner(d) {return d.join('');}},
    {"name": "days$ebnf$1", "symbols": [{"literal":"s"}], "postprocess": id},
    {"name": "days$ebnf$1", "symbols": [], "postprocess": function(d) {return null;}},
    {"name": "days", "symbols": ["days$string$1", "days$ebnf$1"], "postprocess": (d) => "days"},
    {"name": "weeks$string$1", "symbols": [{"literal":"w"}, {"literal":"e"}, {"literal":"e"}, {"literal":"k"}], "postprocess": function joiner(d) {return d.join('');}},
    {"name": "weeks$ebnf$1", "symbols": [{"literal":"s"}], "postprocess": id},
    {"name": "weeks$ebnf$1", "symbols": [], "postprocess": function(d) {return null;}},
    {"name": "weeks", "symbols": ["weeks$string$1", "weeks$ebnf$1"], "postprocess": (d) => "weeks"},
    {"name": "months$string$1", "symbols": [{"literal":"m"}, {"literal":"o"}, {"literal":"n"}, {"literal":"t"}, {"literal":"h"}], "postprocess": function joiner(d) {return d.join('');}},
    {"name": "months$ebnf$1", "symbols": [{"literal":"s"}], "postprocess": id},
    {"name": "months$ebnf$1", "symbols": [], "postprocess": function(d) {return null;}},
    {"name": "months", "symbols": ["months$string$1", "months$ebnf$1"], "postprocess": (d) => "months"},
    {"name": "years$string$1", "symbols": [{"literal":"y"}, {"literal":"e"}, {"literal":"a"}, {"literal":"r"}], "postprocess": function joiner(d) {return d.join('');}},
    {"name": "years$ebnf$1", "symbols": [{"literal":"s"}], "postprocess": id},
    {"name": "years$ebnf$1", "symbols": [], "postprocess": function(d) {return null;}},
    {"name": "years", "symbols": ["years$string$1", "years$ebnf$1"], "postprocess": (d) => "years"},
    {"name": "for_time$subexpression$1", "symbols": ["days"]},
    {"name": "for_time$subexpression$1", "symbols": ["weeks"]},
    {"name": "for_time$subexpression$1", "symbols": ["months"]},
    {"name": "for_time$subexpression$1", "symbols": ["years"]},
    {"name": "for_time", "symbols": ["for", "_", "for_time$subexpression$1"], "postprocess": (d) => (moment().subtract(d[0], d[2]).format(DATE_FORMAT))}
]
  , ParserStart: "date"
}
if (typeof module !== 'undefined'&& typeof module.exports !== 'undefined') {
   module.exports = grammar;
} else {
   window.grammar = grammar;
}
})();
