const nearley = require('nearley');
const _ = require('ramda');
const util = require('util');
const dateGrammar = require('./date');

module.exports = {
  parseDate: s => {
    const parser = new nearley.Parser(nearley.Grammar.fromCompiled(dateGrammar));
    parser.feed(s);
    return _.pipe(_.flatten, _.nth(0))(parser.results);
  },
};
