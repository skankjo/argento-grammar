@builtin "whitespace.ne"
@builtin "number.ne"
@builtin "string.ne"

add -> "add" _  unsigned_decimal _ category _ type _ description _ {%
    (d) => { return d.filter((token) => token !== null)}
%}

category -> [a-zA-Z/]:+ {%
    (d) => { return d[0].join("") }
%}

type -> ("income" | "expense"):? {%
    (d) => { return d[0] || "expense"}
%}

description -> (dqstring | sqstring):*
