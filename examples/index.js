const readline = require('readline');
const { parseDate } = require('../dist/index');

const prompt = readline.createInterface(process.stdin, process.stdout);

const question = s =>
  new Promise((resolve, reject) => prompt.question(s, answer => resolve(answer)));

(async function main() {
  let answer = '';
  while (answer !== 'q') {
    try {
      answer = await question('Give your date: ');
      console.log(parseDate(answer));
    } catch (e) {
      console.log(e.message);
    }
  }
  process.exit();
})();
